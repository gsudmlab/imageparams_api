### What Is This For
This project is created to facilitate accessing the Image Parameter API [http://dmlab.cs.gsu.edu/dmlabapi/](http://dmlab.cs.gsu.edu/dmlabapi/) and provides the following main functionalities:

 1. Retrieving 4096X4096-pixel, aia images in 9 different wavelengths.
 2. Retrieving 10 image parameters computed on any aia image.
 3. Retrieving the heatmap of any image parameter computed on any aia image.

This also provides the user with the possibility of choosing the required arguments as some pre-defined constants, and some validators to verify the format of the arguments.

### How To Use This
This project can be used in EITHER of the following ways: 

 1. Copy all methods from [imageparam_getter.py](aia_image_api/imageparam_getter.py)  into your project. **Note:** You can use `str` instead of any of the following object types: `IMAGE_SIZE`, `IMAGE_PARAM`, and `AIA_WAVE`, as long as they are valid strings.
 
 2. Copy the following 4 modules into your project for full functionality of the project, and then use the api methods in [imageparam_getter.py](aia_image_api/imageparam_getter.py). 
 
     1. [imageparam_getter.py](aia_image_api/imageparam_getter.py) (main accessors)
     2. [arg_validators.py](arg_validation/arg_validators.py) (arguments' validators)
     3. [CONSTANTS.py](constants/CONSTANTS.py) (constants for image-size, wavelength-channel, image-parameter)
     4. [URL_STRINGS.py](constants/URL_STRINGS.py) (base URLs)
     
 3. Clone the repository inside your project, and import the modules.
 
### Usage Example
```python
from datetime import datetime
from constants.CONSTANTS import *
from aia_image_api import imageparam_getter as ipg

dt = datetime.strptime('2012-02-13T20:10:00', '%Y-%m-%dT%H:%M:%S')
aia_wave = AIA_WAVE.AIA_171
image_size = IMAGE_SIZE.P2000
param_id = IMAGE_PARAM.ENTROPY

# -----------------------------------------------
# Retrieve an AIA image:
img = ipg.get_aia_image_jpeg(dt, aia_wave, image_size)
img.show()
# -----------------------------------------------

# -----------------------------------------------
# Retrieve a heatmap of the computed image parameters:
heatmap = ipg.get_aia_imageparam_jpeg(dt, aia_wave, image_size, param_id)
heatmap.show()
# -----------------------------------------------

# -----------------------------------------------
# Retrieve an xml file of the computed image parameters, and convert it to 'ndarray':
xml = ipg.get_aia_imageparam_xml(dt, aia_wave)
res = ipg.convert_param_xml_to_ndarray(xml)
print(res.reshape(64 * 64 * 10).tolist())
# -----------------------------------------------

```
### Requirements
Python version 3.6. (or 2.6)
For a list of required packages, see [requirements.txt](requirements.txt).

### Citation
The Image Parameter dataset is the result of our study that can be cited as follows:

*Ahmadzadeh, Azim and Kempton, J., Dustin, "A Curated Image Parameter Dataset from Solar Dynamics Observatory Mission", Manuscript under review, 2019.*
    
### Author(s)
 * Azim Ahmadzadeh, [aahmadzadeh(_at_)cs(_dot_)gsu(_dot_)edu](aahmadzadeh@cs.gsu.edu)
 
### Acknowledgment
This work was supported in part by two NASA Grant Awards (No. NNX11AM13A, and No. NNX15AF39G), and one NSF Grant Award (No. AC1443061). The NSF Grant Award has been supported by funding from the Division of Advanced Cyberinfrastructure within the Directorate for Computer and Information Science and Engineering, the Division of Astronomical Sciences within the Directorate for Mathematical and Physical Sciences, and the Division of Atmospheric and Geospace Sciences within the Directorate for Geosciences.
 
 
     
 